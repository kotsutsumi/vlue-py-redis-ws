import copy
import json
import threading

import redis
import requests
import time
import uvicorn as uvicorn
from fastapi import FastAPI
# noinspection PyPackageRequirements
from rediscluster import RedisCluster
# noinspection PyPackageRequirements
from starlette.websockets import WebSocket

# FastAPI app
app: FastAPI = FastAPI()

# WebSocket Clients
ws_clients = {}

# Redis Host
redis_host = ["127.0.0.1"]

# Redis port
redis_port = 6379

# Redis Cluster
redis_cluster = False

# Channel Name
redis_channel = "default"

# Redis Server Connection
redis_connection = None

# Redis Database Number
redis_db = 0

# Stock Redis Messages
messages = []

# Pub/Sub Redis Object
pubsub = None

# Authentication Server Host
auth_host = "http://localhost:9888"


# Get Redis Connection
def get_connection():
    # use global params
    global redis_connection, redis_host, redis_host, redis_cluster, redis_db

    if redis_connection is None:

        # create redis connector
        if redis_cluster:

            print(
                f"[connect][cluster] - host: {redis_host}, port: {redis_port}"
            )

            startup_nodes = []
            hosts = redis_host

            if redis_host[0].find(',') != -1:
                hosts = redis_host[0].split(",")

            for host in hosts:
                host = host.strip()

                tmp = host.split(':')

                port = redis_port

                if len(tmp) == 2:
                    host = tmp[0]
                    port = tmp[1]

                startup_nodes.append({"host": host, "port": port})

            r = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)
            redis_connection = r

        else:

            r = redis.Redis(host=redis_host[0].split(':')[0], port=redis_port,
                            db=redis_db)

            print(
                f"[connect] - host: {redis_host[0].split(':')[0]}, port: {redis_port}")

            redis_connection = r

        return r

    return redis_connection


def claim_auth(ws, client_key, channel, data):
    global ws_clients

    # Get Prepared Key
    prepared_key = data['key']

    # Get Remote IP
    remote_ip = data['remote_ip']

    # Create Request Parameters
    params = {
        "type": "auth"
        , "key": prepared_key
        , "remote_ip": remote_ip
    }

    # Send Request to Authentication Server
    response = requests.session().post(auth_host, data=params)

    # JSON decode
    response_data = json.loads(response.text)

    return json.dumps({
        "claim": "auth_response"
        , "client_key": client_key
        , "channel": channel
        , "code": response_data['code']
    })


@app.websocket("/")
async def websocket_endpoint(ws: WebSocket):
    # use globals
    global ws_clients, messages

    # accept WebSocket
    await ws.accept()

    # get client id
    key = ws.headers.get('sec-websocket-key')

    try:
        while True:

            # receive text
            receive_data = await ws.receive_text()

            # JSON decode
            data = json.loads(receive_data)
            # print(f"[receive] - client_key: {key}, msg: {receive_data}")

            if ('claim' in data) is False:
                data['claim'] = None

            if ('channel' in data) is False:
                data['channel'] = None

            # Get Channel
            channel = data['channel']

            # Set init value
            response_text = "{}"

            claim = data['claim']

            # Execute claim
            if claim == 'auth':
                response_text = claim_auth(ws, key, channel, data)

            # Confirm channel in ws_clients
            if (channel in ws_clients) is False:
                ws_clients[channel] = {}

            # Join Channel (add Websocket object)
            ws_clients[channel][key] = ws

            if claim != 'auth_response':

                if redis_cluster:

                    # set text
                    get_connection().set("__redist_ws__", response_text)

                    print(f"[set] - {response_text}")

                    # append text
                    messages.append(get_connection().get("__redist_ws__"))

                else:
                    # publish
                    get_connection().publish(redis_channel, response_text)

                send_messages = copy.copy(messages)
                messages = []

                for index, message in enumerate(send_messages):
                    for client_key in ws_clients[channel].keys():
                        print(
                            f"[send] - client_key: {client_key}, msg: {message}")

                        # send text
                        await ws_clients[channel][client_key].send_text(
                            message)

            time.sleep(5)

    except:
        import traceback
        traceback.print_exc()
        pass

        # close WebSocket connection
        # await ws.close()

        # delete client
        # for client_key in ws_clients.keys():
        #     del ws_clients[channel][client_key]


def subscribe():
    # use global params
    global redis_channel, messages, pubsub

    # subscribe
    pubsub = get_connection().pubsub()
    pubsub.subscribe(redis_channel)

    print(f"[subscribe] - {redis_channel}")

    try:
        for message in pubsub.listen():
            receive_type = message['type']
            receive_channel = message['channel'].decode()

            if receive_type == 'message' and receive_channel == redis_channel:
                # decode text
                data = message['data'].decode()

                print(f"[subscribe] - {data}")

                # append text
                messages.append(data)

    except:
        import traceback
        traceback.print_exc()
        pass


# noinspection PyUnresolvedReferences
@app.on_event("shutdown")
def shutdown_event():
    global pubsub

    # pubsub
    try:
        get_connection().close()
        pubsub.close()
    except:
        pass


class RedisWS:

    # noinspection PyTypeChecker
    @staticmethod
    def run(port, _redis_host, _redis_port, _redis_cluster, _redis_channel,
            _redis_db):
        # use global params
        global redis_host, redis_port, redis_cluster, redis_channel, \
            redis_connection, redis_db

        # set Redis Host
        redis_host = _redis_host

        # set Redis Port
        redis_port = _redis_port

        # set Redis Cluster
        redis_cluster = _redis_cluster

        # set Redis Channel
        redis_channel = _redis_channel

        # start subscribe thread
        if not redis_cluster:
            threading.Thread(target=subscribe, args=[]).start()

        # launch FastAPI
        uvicorn.run(app, host="0.0.0.0", port=port)


if __name__ == '__main__':
    pass

# EOF
