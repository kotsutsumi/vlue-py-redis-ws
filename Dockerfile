FROM python:3.8.5-slim-buster

ENV DEBCONF_NOWARNINGS=yes
ARG REDIS_HOST="127.0.0.1"
ENV REDIS_HOST ${REDIS_HOST}
ARG REDIS_PORT=6379
ENV REDIS_PORT ${REDIS_PORT}
ARG REDIS_DB=0
ENV REDIS_DB ${REDIS_DB}
ARG REDIS_CHANNEL="default"
ENV REDIS_CHANNEL ${REDIS_CHANNEL}
ARG REDIS_CLUSTER=0
ENV REDIS_CLUSTER ${REDIS_CLUSTER}

ADD . /usr/local/app
WORKDIR /usr/local/app
COPY src_old/ /usr/local/app/src

RUN pip --disable-pip-version-check --use-feature=2020-resolver install pip
RUN apt-get update && \
    apt-get -y install redis
RUN pip --disable-pip-version-check --use-feature=2020-resolver install -r requirements.txt

EXPOSE 4000

CMD python src/app.py \
  --redis-host="$REDIS_HOST" \
  --redis-port=$REDIS_PORT \
  --redis-db=$REDIS_DB \
  --redis-channel=$REDIS_CHANNEL \
  --redis-cluster-num=$REDIS_CLUSTER

# EOF