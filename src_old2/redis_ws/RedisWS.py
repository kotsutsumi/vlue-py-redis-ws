from typing import List
from typing import Optional

import uvicorn as uvicorn
from fastapi import Cookie, Depends, FastAPI, Query, WebSocket, status
from fastapi.responses import HTMLResponse

app: FastAPI = FastAPI()


# @app.websocket("/items/{item_id}/ws")

class RedisWS:

    def __init__(self, port, redis_host, redis_port, is_redis_cluster,
                 redis_channel,
                 redis_db):
        # set Redis Host
        self.redis_host = redis_host

        # set Redis Port
        self.redis_port = redis_port

        # set Redis Cluster Flag
        self.is_redis_cluster = is_redis_cluster

        # set Redis Channel
        self.redis_channel = redis_channel

        # set Redis DB
        self.redis_db = redis_db

        # start subscribe thread
        # if not self.is_redis_cluster:
        #     threading.Thread(target=subscribe, args=[]).start()

        # launch FastAPI
        uvicorn.run(app, host="0.0.0.0", port=port)

# EOF
