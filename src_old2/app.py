from typing import List, Optional

import typer

from redis_ws.RedisWS import RedisWS

app = typer.Typer(add_completion=False)


@app.command("main")
def main(
        port: int = typer.Option(
            4000,
            '-P', '--port',
            help='WebSocket Server Listen port number.'
        ),

        redis_host: Optional[List[str]] = typer.Option(
            ["127.0.0.1"],
            help='Connect to Redis Server host.'
        ),

        redis_port: int = typer.Option(
            6379,
            '-RP', '--redis-port',
            help='Redis Server port number.'
        ),

        redis_db: int = typer.Option(
            0,
            '-RD', '--redis-db',
            help='Redis DB number.'
        ),

        redis_channel: str = typer.Option(
            'default',
            '--redis-channel',
            help='Redis channel name.'
        ),

        redis_cluster: bool = typer.Option(
            False,
            '--redis-cluster',
            help='Use Redis Cluser Server.'
        ),

        redis_cluster_num: int = typer.Option(
            -1,
            '--redis-cluster-num',
            help='Use Redis Cluser Server.'
        ),
):
    # override "redis_cluster"
    if redis_cluster_num == 0:
        redis_cluster = False
    elif redis_cluster_num == 1:
        redis_cluster = True

    # Launch RedisWS
    RedisWS(
        port=port,
        redis_host=redis_host,
        redis_port=redis_port,
        is_redis_cluster=redis_cluster,
        redis_channel=redis_channel,
        redis_db=redis_db
    )


if __name__ == "__main__":
    app()

# EOF
