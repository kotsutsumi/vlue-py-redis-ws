# py-redis-ws

py-redis-ws is a `WebSocket Server` that works with `redis`.

also support Redis Cluster using VIP.(still not support specify multi host.)

## Options

- -P, --port
  - WebSocket Server Listen port number.
  - `default`: 4000

- -RH, --redis-host
  - Connect to Redis Server host.
  - `default`: 127.0.0.1

- -RP, --redis-port
  - Redis Server port number.
  - `default`: 6379

- -RD, --redis-db
  - Redis DB number for single redis server.
  - `default`: 0

- --redis-cluster
  - Use Redis Cluster Server.
  - `default`: False

- --redis-channel
  - Redis channel name.
  - `default`: 6379

## usage

for single redis server.

```
python src/app.py
```

for Redis Cluster

```
python src/app.py --redis-host="10.0.0.1:6379, 10.0.0.2:6379, 10.0.0.3:6379" --redis-cluster
```

for Redis Cluster via VIP.
```
python src/app.py --redis-host=`Redis Cluster VIP` --redis-cluster
```

## docker

[https://hub.docker.com/r/kotsutsumi/redis-ws](https://hub.docker.com/r/kotsutsumi/redis-ws)

for single redis server.
```
docker run -d --restart unless-stopped --name="registry.gitlab.com/kotsutsumi/vlue-redis-ws" -p 4000:4000 -e REDIS_HOST="10.0.0.1" kotsutsumi/redis-ws
```

for Redis Cluster via VIP.
```
docker run -d --restart unless-stopped --name="registry.gitlab.com/kotsutsumi/vlue-redis-ws" -p 4000:4000 -e REDIS_HOST="10.0.0.1" -e REDIS_CLUSTER="1" kotsutsumi/redis-ws
```