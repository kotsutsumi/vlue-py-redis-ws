#!/bin/bash

IMAGE="vlue-py-redis-ws"
REGISTRY="registry.gitlab.com/kotsutsumi"
SCRIPT_DIR=$(cd $(dirname "$0") || exit; pwd)
cd "${SCRIPT_DIR}/../" || exit;

docker build -t "${REGISTRY}/${IMAGE}" .
docker login
docker push "${REGISTRY}/${IMAGE}"


# EOF